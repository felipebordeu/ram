#!/usr/bin/env python

from setuptools import setup

setup(name='PhotoRam',
      version='1.1',
      description='A Simple but efficient way to explore and find your pictures',
      author='Felipe Bordeu',
      author_email='FelipeBordeu@gmail.com',
      url='https://gitlab.com/felipebordeu/PhotoRam',
      license="License.txt",
      package_data={'PhotoRam': ['css/*','js/*']},
      include_package_data=True,
      packages=['PhotoRam'],
      package_dir = {'': 'src'}, 
      entry_points={
        'console_scripts': [
            'PhotoRam = PhotoRam.PhotoRamCL:RunFromCommand',
            'PhotoRamGUI = PhotoRam.PhotoRamGUI:RunGUI'
        ],
      },
     )