Random Access Memories 
**********************

A Simple but efficient way to explore and find your pictures

    mamba create -c felipebordeu -n PhotoRam PhotoRam
    mamba activate PhotoRam

to run the Graphical interface run :

    PhotoRamGUI

to run :

    python -m PhotoRAM.PhotoRam

or just 

    PhotoRam


    PhotoRam C:\PRAMTEst\FromE -i --add E:\Foto\Path  --infoNoGeoDate --scan --fromCache  --export htmlMap
        ^                   ^   ^    ^        ^               ^           ^        ^          ^      ^
    PhotoRam                |   |    |________|               |           |   OR   |          |______|
    Database root Directory_|   |             |               |           |________|              |
    Print Database statistics __|             |               |                |                  |
    Add a photo directory  (can be repetend) _|               |                |                  |       
    Print info about missing geoloc and time in shots ________|                |                  |
    Scan directories or load data from cache (use --saveCache after scan) _____|                  |
    Export database in html format at the root of the database ___________________________________|          


You can open the file .RamDB in the root directory to do some manual configuration 

    PhotoRam C:\PRAMTEst\FromE --AddWordToGeoloc placeName:latitude,longitude
        ^                   ^             ^..........^.........^.........^ 
    PhotoRam                |             |          |         |_________|
    Database root Directory_|             |          |              |
    Add folder for word to geoloc mapping_|          |              |
    The word present on the path to the picture _____|              |
    Latitude and longitude to apply to all selected by the work ____|
    
To generate the conda package :

    cd conda
    conda build .