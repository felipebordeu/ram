import os
import json 
import collections
from subprocess import check_output
import datetime


import piexif

#import ffmpeg # for video processing
from PIL import Image
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
from PIL.Image import DecompressionBombError

from  PhotoRam.ExifTools import exif_to_tag, decimal_coords, to_deg, change_to_rational

pictureExtensions = [".jpg", ".png", ".jpeg",".tiff",".bmp", ".mpo",".gif"]
videoExtensions = [".mov",".mp4",".avi",".ogv",".mp",".wmv", ".mpg",".webp", ".3gp",".m4v"]
#

def DictUpdate(d, u):
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            d[k] = DictUpdate(d.get(k, {}), v)
        else:
            if isinstance(v,str) and len(v)==0:
                continue
            if isinstance(v,int) and v == 0:
                continue
            if isinstance(v,float) and v == 0.:
                continue
            d[k] = v
    return d

class Shot:
    __slots__= ('description', 'imagePath', 'thumbnailPath','latitude','altitude','longitude','width','height','picture','photoTakenTimeDateTime','length')
    def __init__(self, description: str="", imagePath: str="", thumbnailPath: str="", latitude:float=0, longitude:float=0 ) -> None:
        self.description = description      # str with the description of the picture
        self.imagePath = imagePath          # absolute path to the image
        self.thumbnailPath = thumbnailPath  # absolute path to the thumbnail
        self.latitude = latitude            # gps latitude
        self.longitude = longitude          # gps longitude
        self.altitude = 0                   # gps altitude
        self.photoTakenTimeDateTime = None  # None of datetime object of the shot taken
        self.width = None                   # with of the shot (pixels)
        self.height = None                  # hight of the shot (pixels)
        self.length = None                  # length in (seconds)
        self.picture =True                  # True if the  shot is a picture (photo) false for Videos
        if os.path.splitext(self.imagePath)[1].lower() in videoExtensions:
            self.picture = False

    def GetYear(self):
        if self.photoTakenTimeDateTime is not None:
            return datetime.datetime.strftime(self.photoTakenTimeDateTime, '%Y')
        else:
            return 0000
        
    def GetFormattedTime(self):
        if self.photoTakenTimeDateTime is not None:
            return self.photoTakenTimeDateTime.isoformat()
        else:
            return ""
        
    def HasGeoLoc(self):
        return  self.latitude != 0. and self.longitude != 0.
    
    def HasDate(self):
        return  self.photoTakenTimeDateTime is not None and self.GetYear() != 0 
    

    def __LoadJson(self, jsonFilename):
        return json.load(open(jsonFilename))
    
    def FillDataFromJson(self,jsonFilename):
        """Given a json files try to fill the metadata of this shot
        """

        # search for the json of the directory 
        #jsonFolder = os.path.join(os.path.dirname(jsonFilename),"Folder.json")
        
        shotMetadata ={}
        shotMetadata["description"] = self.description 
        shotMetadata["geoData"] = {"latitude":self.latitude,"longitude":self.longitude,"altitude":self.altitude}
        shotMetadata["photoTakenTime"] = {"formatted":"","timestamp":0,}
        

        #if os.path.exists(jsonFolder):
        #    DictUpdate(shotMetadata,self.__LoadJson(jsonFolder))
            #shotMetadata.update(self.__LoadJson(jsonFolder))
        DictUpdate(shotMetadata,json.load(open(jsonFilename)))
        #shotMetadataII = json.load(open(jsonFilename))
        #print(jsonFilename)
        
       
        self.description = shotMetadata["description"]
        self.latitude = shotMetadata["geoData"]["latitude"]
        self.longitude = shotMetadata["geoData"]["longitude"]
        self.altitude = shotMetadata["geoData"]["altitude"]

        if self.photoTakenTimeDateTime is None:
            if shotMetadata["photoTakenTime"]["timestamp"] != 0:
                # try using the time stamp
                photoTakenTime  = shotMetadata["photoTakenTime"]["timestamp"]
                self.photoTakenTimeDateTime =  datetime.datetime(1970, 1, 1) + datetime.timedelta(seconds=int(photoTakenTime))
            elif len(shotMetadata["photoTakenTime"]["formatted"]) > 0:
                formattedTime = shotMetadata["photoTakenTime"]["formatted"]
                self.photoTakenTimeDateTime = datetime.datetime.fromisoformat(formattedTime)

    def FillDataFromThumbnail(self,):
        return self.__FillDataFromImage(self.thumbnailPath)
    
    def FillDataFromImageFile(self):
        return self.__FillDataFromImage(self.imagePath)
    
    def __FillDataFromImage(self,imagePath):
        root, ext = os.path.splitext(imagePath)

        if ext.lower() in videoExtensions:
            #TODO scan exif data to retrieve information
            return False
        import PIL
        try:
            image = Image.open(imagePath)
        except DecompressionBombError:
            print(f"DecompressionBombError detected we skip this shot: {imagePath}")
            return 
        
        rawexif = image.info.get('exif')
        if rawexif is None:
            return 
       

        exifTags = exif_to_tag(piexif.load(rawexif))
        zeroth= exifTags["0th"]
        if "Exif" in exifTags:
            exif = exifTags["Exif"]
        else:
            exif = {}

        if 'XResolution' in zeroth:
            self.width = zeroth['XResolution'][0]//zeroth['XResolution'][1]
        
        if 'YResolution' in zeroth:
            self.height = zeroth['YResolution'][0]//zeroth['YResolution'][1]
        
        try:
            if "DateTime" in zeroth and self.photoTakenTimeDateTime is None:
                datetimeData = zeroth['DateTime'] # 2004:01:27 16:42:26
                if "0000:00:00 00:00:00" != datetimeData:
                    self.photoTakenTimeDateTime = datetime.datetime.strptime(datetimeData, '%Y:%m:%d %H:%M:%S')
        except:
            #print(datetimeData)
            pass

        try:
            if "DateTimeOriginal" in exif and self.photoTakenTimeDateTime is None:
                datetimeData = exif['DateTimeOriginal']
                if "0000:00:00 00:00:00" != datetimeData:
                    self.photoTakenTimeDateTime = datetime.datetime.strptime(datetimeData, '%Y:%m:%d %H:%M:%S')
        except:
            #print(datetimeData)
            pass



        if 'GPS' in exif:
            GPSData = exif['GPS']
            if 'GPSLatitude' in GPSData and self.latitude==0:
                self.latitude = decimal_coords([ x[0]/x[1] for x in GPSData['GPSLatitude']], GPSData['GPSLatitudeRef'] )
            
            if self.longitude == 0. and 'GPSLongitude' in GPSData :
                self.longitude = decimal_coords([x[0]/x[1] for x in GPSData['GPSLongitude']], GPSData['GPSLongitudeRef'] )

        if 'GPS' in exifTags:
            GPSData = exifTags['GPS']
            if 'GPSLatitude' in GPSData and self.latitude==0:
                self.latitude = decimal_coords([ x[0]/x[1] for x in GPSData['GPSLatitude']], GPSData['GPSLatitudeRef'] )
            
            if self.longitude == 0. and 'GPSLongitude' in GPSData :
                self.longitude = decimal_coords([x[0]/x[1] for x in GPSData['GPSLongitude']], GPSData['GPSLongitudeRef'] )

        #print(self.longitude)
        #print(self.latitude)
        #print(self.imagePath)
        #print("--------------------------")
        return True

    def SaveMetaDataToJson(self,filename):

        file =open(filename,"wt")
        file.write(json.dumps({"description":self.description,
                               "photoTakenTime":{"formatted":self.GetFormattedTime()},
                                "geoData":{"latitude":self.latitude,"longitude":self.longitude,"altitude":self.altitude}
                               }))
        file.close()

        #if len(self.thumbnailPath):
           
           #pngMetaData = PngImagePlugin.PngInfo() 
 

    def GenerateThumbnail(self,thumbnailFileName):

        root, ext = os.path.splitext(self.imagePath)
        if os.path.exists(thumbnailFileName):
            self.thumbnailPath = thumbnailFileName
            return 
        
        #https://trac.ffmpeg.org/wiki/Scaling
        #Popen(['ffmpeg','-i',self.imagePath,'-vf', "scale=320:240:force_original_aspect_ratio=decrease,pad=320:240:(ow-iw)/2:(oh-ih)/2", thumbnailFileName])
        #self.thumbnailPath =thumbnailFileName
        #return 
        if ext.lower() in videoExtensions:
            #check_output(['ffmpeg','-i', self.imagePath,"-vf", "scale='min(320,iw)':-1" ,"-frames:v","1","-q:v", "2",thumbnailFileName])
            check_output(['ffmpeg','-i', self.imagePath,"-frames:v","1","-q:v", "2",thumbnailFileName])
            #self.thumbnailPath = thumbnailFileName
            imagePath = thumbnailFileName
        else: 
            imagePath = self.imagePath

        try:
            image = Image.open(imagePath)
             
            img_width, img_height = image.size
            self.width = img_width
            self.height = img_height
            aspect_ratio=img_width/img_height
            
            new_width = 300
            new_height = 300

            if img_width > img_height:
                box = (0 + (img_width-img_height)//2 ,0, img_width-(img_width-img_height)//2 ,img_height)
            else:
                box = (0,  + (img_height-img_width)//2 , img_width ,img_height-(img_height-img_width)//2)

            #img_width=max(min(int(img_width/15),310),100)
            #img_height=int(img_width/aspect_ratio)

            imaget = image.resize(((300), (300)),box=box
                                  )#, Image.ANTIALIAS
            try:
                exif = image._getexif()
                
                if exif is not None and  274 in exif:
                    orientation = exif[274]
                    if  orientation == 3 : 
                        imaget=imaget.rotate(180, expand=True)
                        #imageMicro=imageMicro.rotate(180, expand=True)
                    elif orientation == 6 : 
                        imaget=imaget.rotate(270, expand=True)
                        #imageMicro=imageMicro.rotate(270, expand=True)
                        self.width, self.height = self.height, self.width
                    elif orientation == 8 : 
                        imaget=imaget.rotate(90, expand=True)
                        self.width, self.height = self.height, self.width
                        #imageMicro=imageMicro.rotate(90, expand=True)
            except:
                ##problem with exif. Not all format have exif
                pass

            #Save resized images in icons/
            imaget.convert('RGB').save(thumbnailFileName, 'Jpeg', quality=85, exif=self.CreateExifData() )
            #imageMicro.convert('RGB').save(thumbnailFileName[0:-4]+".micro.jpg", 'Jpeg', quality=100)
            self.thumbnailPath = thumbnailFileName
        except DecompressionBombError:
            print(f"DecompressionBombError detected we skip this shot: {self.imagePath}")
            return 
        except:
            print(f"error generating thumbnail for {self.imagePath}")
            raise
            

    def CreateExifData(self):
        import io
        from PIL import Image
        import piexif

        #o = io.BytesIO()
        #thumb_im = Image.open("foo.jpg")
        #thumb_im.thumbnail((50, 50), Image.ANTIALIAS)
        #thumb_im.save(o, "jpeg")
        #thumbnail = o.getvalue()

        zeroth_ifd = {piexif.ImageIFD.Make: u"PhotoRam",
                      piexif.ImageIFD.XResolution: (self.width, 1),
                      piexif.ImageIFD.YResolution: (self.height, 1),
                      piexif.ImageIFD.Software: u"piexif"
                    }
        exif_ifd = {piexif.ExifIFD.DateTimeOriginal: self.GetFormattedTime() }
        lat_deg = to_deg(self.latitude, ["S", "N"])
        lon_deg = to_deg(self.longitude, ["W", "E"])

        exif_lat = (change_to_rational(lat_deg[0]), change_to_rational(lat_deg[1]), change_to_rational(lat_deg[2]))
        exif_lng = (change_to_rational(lon_deg[0]), change_to_rational(lon_deg[1]), change_to_rational(lon_deg[2]))

        gps_ifd = { piexif.GPSIFD.GPSVersionID: (2, 0, 0, 0),
                    piexif.GPSIFD.GPSLongitudeRef: lon_deg[3],
                    piexif.GPSIFD.GPSLongitude: exif_lng,
                    piexif.GPSIFD.GPSLatitudeRef: lat_deg[3],
                    piexif.GPSIFD.GPSLatitude: exif_lat,
                    piexif.GPSIFD.GPSAltitude: change_to_rational(round(abs(self.altitude)*100)/100),
                    piexif.GPSIFD.GPSAltitudeRef: 0, # 1 is under the see
                    }
        
        first_ifd = {} #piexif.ImageIFD.Make: u"Canon",

        exif_dict = {"0th":zeroth_ifd, "Exif":exif_ifd, "GPS":gps_ifd, "1st":first_ifd, }#"thumbnail":thumbnail
        exif_bytes = piexif.dump(exif_dict)
        return exif_bytes
