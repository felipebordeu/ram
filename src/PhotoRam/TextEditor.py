import tkinter as tk
from tkinter import filedialog
import re


class Window(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.master = master

        self.master.title("PhotoRam Config Editor")
        self.pack(fill=tk.BOTH, expand=1)

        top = master
        menu = tk.Menu(top)
        top.config(menu=menu)
        self.file_menu = tk.Menu(menu)
        menu.add_cascade(label="File", menu=self.file_menu)
        # self.file_menu.add_command(label="New")
        # self.file_menu.add_command(label="Open", command=self.open_file_function)
        self.file_menu.add_command(label="Save", command=self.save_file_function)
        self.file_menu.add_separator()
        # self.file_menu.add_command(label="Exit")

        self.text = tk.Text(
            top, height=200, width=200
        )  # Use Text widget insted of Listbox
        self.text.pack(side=tk.LEFT, fill=tk.Y, expand=True)

        self.scrollbar = tk.Scrollbar(top, orient="vertical")
        self.scrollbar.config(command=self.text.yview)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y, expand=True)

        # change all occurances of self.listNodes to self.text
        self.text.config(yscrollcommand=self.scrollbar.set)

        master.geometry("1000x300")

    def open_file_function(self, filename=None, initialdir="/"):

        if filename is None:
            self.file_save = tk.filedialog.askopenfilename(
                initialdir=initialdir,
                title="Select file",
                filetypes=(
                    ("PhotoRam Config files", "*.RamConfig"),
                    ("All files", "*.*"),
                ),
            )
        else:
            self.file_save = filename

        self.master.title("PhotoRam Config Editor: " + str(self.file_save))

        with open(self.file_save) as file:
            for i in file:
                self.text.insert(tk.END, i)

    def save_file_function(self):
        text = self.text.get(1.0, tk.END)

        with open(self.file_save, mode="wt") as file:
            file.write(text)
