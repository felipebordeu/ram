import sys
import os
import datetime
import glob
import shutil
import re 
import pickle
import json

# Import libraries

from PhotoRam.Shot import Shot, pictureExtensions, videoExtensions

NameToGeo= {}

allExtensions = []
allExtensions.extend(pictureExtensions)
allExtensions.extend(videoExtensions)

NotNumberRight = "(?![0-9])"
NotNumberLeft = "(?<![0-9])"
YearReg = "(?P<Year>[1][9][8-9][0-9]|[2][0][0-5][0-9])"
MonthReg = "(?P<Month>[0][0-9]|[1][0-2])"
DayReg = "(?P<Day>[0-2][0-9]|[3][0-1])"

class DataBase:
    def __init__(self,rootPath):
        self.print = print
        self.rootPath = rootPath
        self.config = {"PhotoRoots":{}, "PathWordsToGeoLoc":{}, "PathWordsToDate":{} , "RegsToDate":{} }
        self.SetRootPath(rootPath)
        self.ForceLoadDataFromOriginalFolder = False
        self.verbose = True
        
    
    def __str__(self):
        res = ""
        res +=  "Photo RAM DataBase : \n"
        res += f"  root path : {self.rootPath}  \n"
        res += f"  config file : {self.GetDataBaseConfigFile()} \n" 
        res +=  "  roots : \n"
        if len(self.config["PhotoRoots"]):
            for k,root in self.config["PhotoRoots"].items():
                res +=  f"     - ({k}) {root}\n"
        else: 
            res +=  "     (no roots) \n"
            
        if len(self.config["PathWordsToGeoLoc"]):
            res +=  f"  Word to Geolocalization \n"

            for site, loc in self.config["PathWordsToGeoLoc"].items():
                res +=  f"     {site}: {loc} \n"

        if len(self.config.get("PathWordsToDate",{})):
            res +=  f"  Word to timeStamp \n"
            for site, loc in self.config["PathWordsToDate"].items():
                res +=  f"     {site}: {loc} \n"

        if len(self.config.get("RegsToDate",{})):
            res +=  f"  Word to timeStamp \n"
            for reg, active in self.config["RegsToDate"].items():
                res +=  f"     {reg}: {active} \n"

        return res

    def Print(self, *args, **nargs):
        if self.verbose:
            self.print(*args, **nargs)

    def SetRootPath(self, rootPath):
        if not os.path.isdir(rootPath):
            raise RuntimeError("need a directory")
        self.rootPath = rootPath

        if os.path.isfile(self.GetDataBaseConfigFile()):
            self.__ReadConfigFile()
            self.print("Config File Read")
        else:
            self.print("Non config file ")

    def GetDataBaseConfigFile(self):
        return os.path.join(self.rootPath,".RamDB",".RamConfig")
    
    def __ReadConfigFile(self):
        self.config = json.load(open(self.GetDataBaseConfigFile()))
        self.config["PhotoRoots"] = { k:os.path.normpath(path) for k,path in self.config["PhotoRoots"].items() }

    def ReadCache(self):
        cacheFileName = os.path.join(self.rootPath,".RamDB", "shots.pickle")
        
        if os.path.exists(cacheFileName):
            return pickle.load(open(cacheFileName,"rb"))
        else:
            self.print("no Cache available")
            return []

    def SaveCache(self, shots):
        cacheFileName = os.path.join(self.rootPath,".RamDB", "shots.pickle")
        pickle.dump(shots, open(cacheFileName,"wb"))

    def SaveConfigFile(self):
        if not os.path.isdir(os.path.join(self.rootPath,".RamDB")):
            os.mkdir(os.path.join(self.rootPath,".RamDB"))

        file = open(self.GetDataBaseConfigFile(),"wt")
        file.write(json.dumps(self.config,check_circular =False,indent=2) )

    def AddPath(self, path):
        if not os.path.isdir(path):
            raise RuntimeError("need a directory")
        
        path = os.path.normpath(path)
        if path in self.config["PhotoRoots"].values():
            return 
        
        for i in range(1000):
            if str(i) not in self.config["PhotoRoots"].keys():
                self.config["PhotoRoots"][str(i)] = path
                break
        else:
            raise RuntimeError("Cant work on more than a 1000 roots")

        self.SaveConfigFile()

    def AddWordToGeoloc(self, site, latitude_longitude):
        self.config["PathWordsToGeoLoc"][site] = latitude_longitude
        self.SaveConfigFile()

    def AddWordToDate(self, site, timeStamp):
        self.config["PathWordsToDate"][site] = timeStamp
        self.SaveConfigFile()

    def CleanDataBase(self):
        for key,pathToPhotos in self.config["PhotoRoots"].items():
            pathToOutputResources = os.path.join(self.rootPath,".RamDB",key) 

            for filename in glob.glob(pathToOutputResources+os.sep+"**",recursive=True):
                if os.path.isdir(filename):
                    continue
                #if filename == "Folder.json":
                #    continue

                root, ext = os.path.splitext(filename)
                rootfile = os.path.join(pathToPhotos, os.path.relpath(root, pathToOutputResources))
               
                if os.path.exists(rootfile):
                    continue
                
                self.Print(f"found file {filename} but the files {rootfile} does not exists. Deleting it...")
                if filename.find('.RamDB') > -1 :
                    os.remove(filename)

    def ScanPhotoRoots(self, stopAt= 1e12) :
        allShots = []

        for key,pathToPhotos in self.config["PhotoRoots"].items():
            pathToPhotos = os.path.normpath(pathToPhotos)
            pathToOutputResources = os.path.normpath(os.path.join(self.rootPath,".RamDB",key) )
            if not os.path.isdir(pathToOutputResources):
                os.mkdir(pathToOutputResources)

            for filename in glob.iglob(pathToPhotos+os.sep+"**",recursive=True):
                
                if os.path.isdir(filename):
                    self.Print(filename)
                    continue
                
                onlyFilename =  os.path.basename(filename)
                root, ext = os.path.splitext(onlyFilename)
                
                relativePathToDataBaseRoot = os.path.relpath(filename, pathToPhotos)
                relativeDirectory = os.path.dirname(relativePathToDataBaseRoot)
                DBDirectory = os.path.join(pathToOutputResources,relativeDirectory)
                DBFile =  os.path.join(pathToOutputResources,relativePathToDataBaseRoot)
                folderJson = os.path.join(os.path.dirname(filename),"Folder.json")
                if not os.path.isdir(DBDirectory):
                    os.makedirs(DBDirectory)

                if ext.lower() not in allExtensions:
                    #if onlyFilename == "Folder.json":
                    #    relativePathToDataBaseRoot =DBFile
                    #    if os.path.exists(DBFile):
                    #        os.remove(DBFile)
                    #    shutil.copyfile(filename, DBFile)
                    #    continue
                    if filename.endswith(".json"):
                        continue
                    else:
                        self.Print(f"skip file {filename}")
                    continue

                data = Shot(imagePath=filename)
                
                
                #ramLocalJsonFileName = DBFile +".json"

                needToSaveJson = True
                #if os.path.isfile(ramLocalJsonFileName) and  not self.ForceLoadDataFromOriginalFolder :
                #    data.FillDataFromJson(ramLocalJsonFileName)
                #    needToSaveJson = False

                thumbnailFileName = DBFile+".jpg"


                if os.path.isfile(thumbnailFileName) and  not self.ForceLoadDataFromOriginalFolder :
                    data.thumbnailPath = thumbnailFileName
                    data.FillDataFromThumbnail()
                    #print(data.longitude)
                    #print(data.thumbnailPath)
                    
                else:
                    #find directory json
                    self.Print(filename)
                    #print(filename+ ".json")
                    
                    if os.path.isfile(folderJson):
                        data.FillDataFromJson(folderJson)
                    if os.path.isfile(filename+ ".json"):
                        data.FillDataFromJson(filename+ ".json")
                    
                    needToSaveJson = data.FillDataFromImageFile()
                    #print(data.longitude)
                    data.GenerateThumbnail(thumbnailFileName) 
                    #print(data.longitude)
                    
                
                
                data.thumbnailPath = os.path.normpath(os.path.join(".RamDB",key,relativePathToDataBaseRoot+".jpg"))

                #if data.HasGeoLoc():
                allShots.append(data)

                if len(allShots )>= stopAt:
                    break
        self.Print("Done Scanning for shots")
        return allShots
  
    
        templateHead = """
<!DOCTYPE html>
<html>
  <head>
    <title>Test Gallery</title>
  </head>
  <body>

    <script src="./photoswipe.umd.min.js"></script>
    <script src="./photoswipe-lightbox.umd.min.js"></script>
    
    <link rel="stylesheet" href="./photoswipe.css">

    <div class="test-gallery">"""
        
        templatePhoto =  """
      <a href="{imagePath}" data-pswp-width="{width}" data-pswp-height="{height}">
        <img src="{thumbnailPath}" alt="" />
      </a>
      """
        templateTail = """
    </div>
    
    <script type="text/javascript">
      var lightbox = new PhotoSwipeLightbox({
        gallery: '.test-gallery',
        children: 'a',
        // dynamic import is not supported in UMD version
        pswpModule: PhotoSwipe 
      });
      lightbox.init();
    </script>

  </body>
</html>
        """
        file = open(os.path.join(self.rootPath,outputName+".html"),"wt")
        file.write(templateHead)
        for shot in allShots:
            thumbnailPath = shot.thumbnailPath.replace("\\","/")
            description = shot.description 
            imagePath = shot.imagePath.replace("\\","/")
            file.write(templatePhoto.format(thumbnailPath=thumbnailPath, imagePath=imagePath,width=shot.width,height=shot.height))
        file.write(templateTail)
        file.close()
   
    def AddGeolocationFromAlmanac(self, shots, almanac=None):
        cpt = 0
        if almanac is None:
            almanac = self.config["PathWordsToGeoLoc"]
        for shot in shots:
            for k,v in almanac.items():
                if k in shot.imagePath:
                    shot.latitude = v[0]
                    shot.longitude = v[1]
                    cpt += 1
        self.Print(f"Geolocalization updated on {cpt} shots")

    def AddDateFromAlmanac(self, shots, almanac=None):
        cpt = 0
        if almanac is None:
            almanac = self.config.get("PathWordsToDate",{})
        
        for shot in shots:
            if shot.HasDate():
                continue
            for k,v in almanac.items():
                if k in shot.imagePath:
                    shot.photoTakenTimeDateTime = datetime.datetime.fromisoformat(v)
                    cpt += 1
        
        almanac = self.config.get("RegsToDate",{})
        regAlmanac = []
        for k,v in almanac.items():
            if v:
                regAlmanac.append(re.compile(k.format(NL=NotNumberLeft,Day=DayReg,Month=MonthReg,Year=YearReg,NR=NotNumberRight)))
            else:
                continue

        for shot in shots:
            if shot.HasDate():
                continue
            for reg in regAlmanac:
                res = re.search(reg,shot.imagePath)
                if res:
                    try:
                        Year = int(res.group("Year"))
                    except :
                        Year = 1980

                    try:
                        Month = int(res.group("Month"))
                    except :
                        Month = 1
                    try:
                        Day = int(res.group("Day"))
                    except :
                        Day = 1
                    try:
                        date = datetime.date(Year,Month,Day)
                    except:
                        continue

                    shot.photoTakenTimeDateTime = date
                    cpt += 1 
                    break


        self.Print(f"Date updated on {cpt} shots")
        


    def AddTimeFromAlmanac(self, shots, almanac, addWatsappDat=False):
        # from https://stackoverflow.com/questions/33031663/how-to-change-image-captured-date-in-python

        cpt = 0
        photoTakenTime = []
        for shot in shots:
            if shot.photoTakenTime == 0:
                imagePath = self.imagePath    
                if addWatsappDat and re.match(r"^IMG-\d\d\d\d\d\d\d\d-WA\d\d\d\d.*", imagePath):
                    match = re.search(r"^IMG-(\d\d\d\d)(\d\d)(\d\d)-WA\d\d\d\d.*", imagePath)
                year = match.group(1)
                month= match.group(2)
                day = match.group(3)
                datetime.date.fromtimestamp(int(timestamp))
                datetime.date


                #print(shot.imagePath)
                continue
        #photoTakenTime.append(shot.photoTakenTime)
        #cpt += 1
        #if len(photoTakenTime) > 100:
        #    print(photoTakenTime)
        #    photoTakenTime = []

       

        
        self.Print(f"Time updated on {cpt} shots")

    def PrintStatistics(self, shots: list[Shot]):        
        notGeoShot = 0
        words = set()
        for shot in shots:
            if not shot.HasGeoLoc():
                notGeoShot += 1
                words.update(os.path.dirname(shot.imagePath).split(os.sep))

        self.print(f" Total number of shots {len(shots)}")
        self.print(f"    Number of shot without Geoloc: {notGeoShot}")
        self.print(words)

