import os
import sys 
import json

import folium
from folium.plugins import HeatMap
from branca.element import Element, JavascriptLink, CssLink


def ExportDataBaseInJavaScriptAssetsFlat(rootPath, shots, imagesTimeGroups):
    """Export the shots and time groups to a json file"""

    javascriptData = open(os.path.join(rootPath, "databaseFlat.js"), "w")
    javascriptData.write(f"const allimages = [\n")
    for i, shot in enumerate(shots):
        thumbnailPath = shot.thumbnailPath.replace("\\", "/")
        imagePath = shot.imagePath.replace("\\", "/")
        width = shot.width
        width = width if width else 300

        height = shot.height
        height = height if height else 300
        year = shot.GetYear()
        FormattedTime = shot.GetFormattedTime()
        javascriptData.write(
            f"""{{ id: {i+1}, thum: {json.dumps(thumbnailPath) }, src: {json.dumps(imagePath)}, width: {width}, height: {height}, year:{year} , date:"{shot.GetFormattedTime()}", latitude:{shot.latitude}, longitude:{shot.longitude}, picture:{"true" if shot.picture else "false"} }},\n"""
        )
    javascriptData.write(f"] \n")
    javascriptData.write(f"const imagesTimeGroups = [\n")

    javascriptData.write(
        ",".join(map(str, sorted(map(int, imagesTimeGroups)))) + " ] \n"
    )

    javascriptData.close()


def ExportFolium(rootPath,  allShots,  outputName="RamWebSite",xDefaultLocation=45.783, yDefaultLocation=4.877, verbose=False, print=print):

    def Print(data):
        if verbose:
            print(data)

    tiles = "OpenStreetMap"
    # tiles = "cartodb positron"
    # tiles = "cartodbdark_matter"
    map_osm = folium.Map(
        location=[xDefaultLocation, yDefaultLocation],
        zoom_start=5,
        min_zoom=3,
        max_zoom=30,
        tiles=tiles,
        control_scale=True,
        attr="TEST",
        className="map-tiles",
    )  # Star location when opening the map html

    map_osm._id = "mainFoliumMap"

    from jinja2 import Template

    map_osm._template = Template(
        """
        {% macro header(this, kwargs) %}
            <meta name="viewport" content="width=device-width,
                initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

        {% endmacro %}

        {% macro html(this, kwargs) %}
        
        
        
        
        <div class="myrow">
        <div class="folium-map" id="map_mainFoliumMap" ></div>
        <div class="pswp-right--gallery" id="right-panel-gallery" >
            </div><div class="control-panel">
                <div class="mode center"><span class="change">Light</span></div>
                
                <div class="center">
                    <div>
                        <input class="vslider" id="space-range" type="range" orient="vertical" value="20" min="1" max="1000" oninput="RadiusChanged()">
                    </div>
                    <div>
                        <output class="text" id="radiusOutput" >20 meters</output>
                    </div>
                </div>

                <div class="center">
                    <div>
                        <input class="vslider" id="time-range-start" type="range" orient="vertical" value="0" min="0" max="21" oninput="TimeChanged()">
                        <input class="vslider" id="time-range-stop" type="range" orient="vertical" value="21" min="0" max="21" oninput="TimeChanged()">
                    </div>
                    <div>
                        <output class="text" id="timeOutputStart">20 days</output>-<output class="text" id="timeOutputStops">20 days</output>
                    </div>
                    <div>
                        <output class="text" id="outputText">Welcome</output>
                    </div>
                </div>
                
            </div>
         </div> 
        
        {% endmacro %}

        {% macro script(this, kwargs) %}
            var {{ this.get_name() }} = L.map(
                {{ this.get_name()|tojson }},
                {
                    center: {{ this.location|tojson }},
                    crs: L.CRS.{{ this.crs }},
                    {%- for key, value in this.options.items() %}
                    {{ key }}: {{ value|tojson }},
                    {%- endfor %}
                }
            );

            {%- if this.control_scale %}
            L.control.scale().addTo({{ this.get_name() }});
            {%- endif %}

            {% if this.objects_to_stay_in_front %}
            function objects_in_front() {
                {%- for obj in this.objects_to_stay_in_front %}
                    {{ obj.get_name() }}.bringToFront();
                {%- endfor %}
            };
            {{ this.get_name() }}.on("overlayadd", objects_in_front);
            $(document).ready(objects_in_front);
            {%- endif %}

        {% endmacro %}
        """
    )

    # this are the photoswipe assets
    #JavascriptLink(url="./photoswipe-lightbox.umd.min.js")
    #map_osm.get_root().html.add_child(JavascriptLink(url="./photoswipe.umd.min.js"))
    map_osm.get_root().html.add_child(JavascriptLink(url="http://malsup.com/jquery/hoverpulse/jquery.hoverpulse.js"))
    map_osm.get_root().html.add_child(JavascriptLink(url="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/5.4.2/umd/photoswipe.umd.min.js"))
    map_osm.get_root().html.add_child(JavascriptLink(url="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/5.4.2/umd/photoswipe-lightbox.umd.min.js"))
    map_osm.get_root().html.add_child(CssLink(url="https://cdnjs.cloudflare.com/ajax/libs/photoswipe/5.4.2/photoswipe.min.css"))
    map_osm.get_root().html.add_child(CssLink(url="./PhotoRam.css"))

    # this is the database off all the photos
    # map_osm.get_root().header.add_child(JavascriptLink(url="./database.js"))
    map_osm.get_root().header.add_child(JavascriptLink(url="./databaseFlat.js"))

    # generation of the heatmap
    data1 = [
        [x.latitude, x.longitude]
        for x in allShots
        if x.latitude != 0.0 and x.longitude != 0.0
    ]  # Specification data format
    HeatMap(data1, name="Heat Map").add_to(
        map_osm
    )  # Add heat map to the previously created map

    # For each image, generate a marker with the respective popup image (That was smaller than the original one)

    from folium.plugins import MarkerCluster

    marker_cluster = MarkerCluster(name="Photos")
    map_osm.add_child(marker_cluster)

    groups = {}
    imagesTimeGroups = set()

    def addToGroup(marker, pretty_date):
        if pretty_date not in groups:
            # g = folium.FeatureGroup(name = pretty_date)
            g = folium.plugins.FeatureGroupSubGroup(marker_cluster, pretty_date)
            Print(f"creating of layer for year {pretty_date}")
            # g.add_to(map_osm)
            map_osm.add_child(g)
            groups[pretty_date] = g

        else:
            g = groups[pretty_date]
        marker.add_to(g)

    from collections import defaultdict

    geoShots = defaultdict(list)
    notGeoShot = 0
    for shot in allShots:
        latitude = round(shot.latitude, 4)
        longitude = round(shot.longitude, 4)
        if latitude == 0.0 and longitude == 0.0:
            notGeoShot += 1
            Print(shot.imagePath)
            continue

        geoShots[(latitude, longitude)].append(shot)
        imagesTimeGroups.add(shot.GetYear())

    for key, shots in geoShots.items():
        latitude = key[0]
        longitude = key[1]
        for shot in shots:
            thumbnailPath = shot.thumbnailPath.replace("\\", "/")
            imagePath = shot.imagePath.replace("\\", "/")

            latitude = round(shot.latitude, 4)
            longitude = round(shot.longitude, 4)
            images_hash = f"""images_{hash(latitude) % ((sys.maxsize + 1) * 2)}{hash(longitude) % ((sys.maxsize + 1) * 2)}"""
            # class="pswp-gallery" id="gallery--mixed-source"
            start = f"""<div > \n 
                <a onclick="return RunDiapo({latitude},{longitude})"
                data-id="1" 
                href="{imagePath}" 
                target="_blank">
                <img src="{thumbnailPath}" alt="">
                </a>
                </div>"""
            break

            #                if ft:
            #                    start += f"""<div><a href="{imagePath}"><img src="{thumbnailPath}" alt="Image 001" /></a></div>"""
            #                    ft=False
            #                else:
            #                    start += f"""<div class="hidden"><a href="{imagePath}"></a></div>"""

            # width = shot.width
            # height = shot.height
            description = shot.description
            # html = f''' <a href="{imagePath}" target="_blank" > <img src="{thumbnailPath}" class="center" ></a>
            # <figcaption>{shot.GetFormattedTime()} <a href="{imagePath}" target="_blank" >Original</a> {description}</figcaption>'''
        # latitude = key[0]
        # longitude = key[1]

        popup = folium.Popup(start, max_width=2650)
        if shot.picture:
            icon = "glyphicon glyphicon-picture"
        else:
            icon = "glyphicon glyphicon-film"
        m = folium.Marker(
            location=[latitude, longitude],
            popup=popup,
            icon=folium.Icon(color="green", icon=icon),  # glyphicon glyphicon-film
            lazy=True,
            tooltip=f"{len(shots)} Photos",
        )
        ## to put all the photos in the same layer
        # m.add_to(marker_cluster)
        # to sort photos by year in separate layers

        # m.add_to(marker_cluster)
        addToGroup(m, shot.GetYear())
    ExportDataBaseInJavaScriptAssetsFlat(rootPath , allShots, imagesTimeGroups)
    lc = folium.LayerControl()
    lc.add_to(map_osm)
    folium.FitOverlays().add_to(map_osm)

    # add popup for latitude and logitude
    popup1 = folium.LatLngPopup()
    popup1._template = Template(
        """
            {% macro script(this, kwargs) %}
                var {{this.get_name()}} = L.popup();
                function latLngPop(e) {
                       RunDiapo(e.latlng.lat,e.latlng.lng);
                       {{this.get_name()}}
                        .setLatLng(e.latlng)
                        .setContent("<span> Latitude: " + e.latlng.lat.toFixed(5) +
                                    "<br>Longitude: " + e.latlng.lng.toFixed(5) +
                                    "<br> <span><br> <button class='button' onclick='return SendToClipboard("+e.latlng.lat+","+e.latlng.lng+")'> copied to the clipboard </button><br><button class='button' onclick='return RunDiapo("+e.latlng.lat+","+e.latlng.lng+")'> Find Pictures</button>"  )
                        .openOn({{this._parent.get_name()}});
                        
                    }
                {{this._parent.get_name()}}.on('click', latLngPop);
            {% endmacro %}
            """
    )

    map_osm.add_child(popup1)

    import shutil

    shutil.copyfile(
        os.path.join(os.path.dirname(__file__), "js", "PhotoRam.js"),
        os.path.join(rootPath, "PhotoRam.js"),
    )
    shutil.copyfile(
        os.path.join(os.path.dirname(__file__), "css", "PhotoRam.css"),
        os.path.join(rootPath, "PhotoRam.css"),
    )

    map_osm.get_root().html.add_child(JavascriptLink(url="./PhotoRam.js"))

    map_osm.save(os.path.join(rootPath, outputName + ".html"))  # Save as html file
    Print(f"found {notGeoShot} shots without geo-localization, theses shots will not be exported")
    Print(f"WebPage generation Done")
    Print(f"{os.path.join(rootPath, outputName + ".html")}")