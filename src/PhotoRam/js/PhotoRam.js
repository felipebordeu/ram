
/* Get distance for 2 GPS points strait over the sphere  */
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
  var R = 6371 * 1000; // Radius of the earth in m
  var dLat = deg2rad(lat2 - lat1);  // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in m
  return d;
}
/* degrees to radians */
function deg2rad(deg) {
  return deg * (Math.PI / 180)
}

/* return the space interval in meters*/
function GetSpaceRange() {
  const metersDistance = parseInt(document.getElementById("space-range").value);
  if (800 < metersDistance) {
    return parseInt(800 + ((metersDistance - 800)) ** 2)
  }
  return parseInt(metersDistance)
}
/**return the time interval in years*/
function GetTimeInterval() {
  const timeStart = imagesTimeGroups[parseInt(document.getElementById("time-range-start").value)]
  const timeStop = imagesTimeGroups[parseInt(document.getElementById("time-range-stop").value)]
  return [timeStart, timeStop]
}

/**count the number of images around the mouse pointer */
function CountShotsPointer(latitude, longitude) {
  var cpt = 0
  const [timeStart, timeStop] = GetTimeInterval()
  const metersDistance = GetSpaceRange();

  for (shot of allimages) {
    if (shot.year >= timeStart && shot.year <= timeStop) {
      if (getDistanceFromLatLonInKm(latitude, longitude, shot.latitude, shot.longitude) < metersDistance) {
        cpt += 1
      }
    }
  }
  return cpt
}

/* store partial (time) and final filtered list */
var timeFilteredShots = [];
var spaceTimeFilteredShots = [];
var storedLatLong = [0, 0]

/** sort selection by time and store it in the global variable  timeFilteredShots*/
function FilterShotsByTime() {
  const [timeStart, timeStop] = GetTimeInterval()
  timeFilteredShots = allimages.filter((shot) => shot.year >= timeStart && shot.year <= timeStop);
}
/** filter selection by space store in the global variable spaceTimeFilteredShots */
function FilterShots(latitude, longitude) {
  if (latitude == 0 && longitude == 0) {
    [latitude, longitude] = storedLatLong
  } else {
    storedLatLong = [latitude, longitude]
  }
  if (latitude == 0 && longitude == 0) {
    spaceTimeFilteredShots = []
    return
  }
  FilterShotsByTime()
  metersDistance = GetSpaceRange();
  spaceTimeFilteredShots = timeFilteredShots.filter((shot) => getDistanceFromLatLonInKm(latitude, longitude, shot.latitude, shot.longitude) < metersDistance)
}

/** fill the swipe panel with the selection  */
function RunDiapo(latitude, longitude) {
  FilterShots(latitude, longitude)
  FillRightPanel(spaceTimeFilteredShots);
  return false;
}

/**Send the lat and lng to clipboard for faster path to gps */
function SendToClipboard(lat, lng){
  text =  ', "": [' + lat +", "+lng+" ] "
  navigator.clipboard.writeText(text)
}
/** to retrieve left and right panels */
function GetLeftPanel() {
  return document.getElementById("mainFoliumMap");
}
function GetRightPanel() {
  return document.getElementById("right-panel-gallery");
}

/** Photo Swipe Engine */
const lightbox = new PhotoSwipeLightbox({
  //dataSource: []
  gallery: '.pswp-right--gallery',
  children: 'a',
  thumbSelector: 'a',
  pswpModule: PhotoSwipe
});

// work for custom content
// Parse data-picture attribute
lightbox.addFilter('itemData', (itemData, index) => {
  itemData.picture = itemData.element.dataset.picture;
  return itemData;
});

// use <picture> instead of <img>
lightbox.on('contentLoad', (e) => {
  const { content, isLazy } = e;

  if (content.data.picture=="false") {
    // prevent to stop the default behavior
    e.preventDefault();

    content.pictureElement = document.createElement('video');
    content.pictureElement.controls = true   
    //content.pictureElement.autoplay = true   
    content.pictureElement.height= content.data.height
    content.pictureElement.width = content.data.width
    content.pictureElement.classList.add('pswp__img');
    content.pictureElement.classList.add('demoVideo');
    
    //content.pictureElement.preload = 'none'

    const sourceVideo = document.createElement('source');
    sourceVideo.src = content.data.src
    sourceVideo.type = 'video/'+ content.data.src.split('.').pop().toLowerCase();
    //alert(sourceVideo.src)
    //alert(sourceVideo.type)

    const sourceJpg = document.createElement('source');
    sourceJpg.src = content.data.msrc;
    sourceJpg.type = 'image/jpeg';
    //alert(sourceJpg.src)

    content.element = content.pictureElement 
    elem = document.createElement('img');
    elem.src = content.data.msrc;
    elem.setAttribute('alt', '');
    elem.className = 'pswp__img';
    
    content.pictureElement.appendChild(sourceJpg);
    content.pictureElement.appendChild(sourceVideo);
    content.pictureElement.appendChild(elem);

    
    content.state = 'loading';

    if (content.element.complete) {
      content.onLoaded();
    } else {
      content.element.onload = () => {
        content.onLoaded();
      };

      content.element.onerror = () => {
        content.onError();
      };
    }
    
  }
});

lightbox.on('contentActivate', (e ) => {
  // content becomes active (the current slide)
  // can be default prevented
  const { content } = e;
  if (content.pictureElement ) {
    //&& !content.pictureElement.parentNode
    content.pictureElement.play()
  }
});

lightbox.on('contentDeactivate', (e) => {
  const { content } = e;
  if (content.pictureElement ) {
    //&& !content.pictureElement.parentNode
    content.pictureElement.pause()
  }
})

// by default PhotoSwipe appends <img>,
// but we want to append <picture>
lightbox.on('contentAppend', (e) => {
  const { content } = e;
  if (content.pictureElement && !content.pictureElement.parentNode) {
    e.preventDefault();
    content.slide.container.appendChild(content.pictureElement);
  }
});

// for next/prev navigation with <picture>
// by default PhotoSwipe removes <img>,
// but we want to remove <picture>
lightbox.on('contentRemove', (e) => {
  const { content } = e;
  if (content.pictureElement && content.pictureElement.parentNode) {
    content.pictureElement.pause()
    e.preventDefault();
    content.pictureElement.remove();
  }
});


lightbox.init();

/** file the photo  */
function FillRightPanel(source) {
  document.getElementById("outputText").value = (source.length).toString() + " Shots"
  const sortedSource = source.sort((a, b) => a.year - b.year)
  rp = GetRightPanel()
  innerHTML = '';
  cpt = 0
  if (sortedSource.length == 0){
    rp.innerHTML = ''
    return 
  }
  memDate = sortedSource[0].year
  innerHTML += "<div> " + memDate + "</div>"
  for (shot of sortedSource) {
    if (memDate < shot.year) {
      memDate = shot.year
      innerHTML += "<div class='photodate'> " + memDate + "</div>"
    }
    innerHTML += '<a onclick="lightbox.loadAndOpen(' + cpt + ');return false;" data-id="' + cpt + '"  data-picture="'+shot.picture+'"  href="' + shot.src + '" target="_blank" data-pswp-width="' + shot.width + '" data-pswp-height="' + shot.height + '"><img class="thumclass" src="' + shot.thum + '" alt=""></a>'
    cpt += 1
  }
  rp.innerHTML = innerHTML

  lightbox.options.dataSource = source;
}

window.onload = function () {

  /*Dark light control */
  $(".change").on("click", function () {
    if ($("body").hasClass("dark")) {
      $("body").removeClass("dark");
      $(".change").text("Light");
    } else {
      $("body").addClass("dark");
      $(".change").text("Dark");
    }

    if ($(".leaflet-tile-pane").hasClass("dark-filter") ){
      $(".leaflet-tile-pane").removeClass("dark-filter");
    }else{
      $(".leaflet-tile-pane").addClass("dark-filter");
    }

  });

  map = map_mainFoliumMap;

  var circle = L.circle(map.getCenter(), {
    radius: 20,
    color: 'red',
    fillOpacity: 0.2,
  }).addTo(map);


  document.getElementById("time-range-start").min = 0
  document.getElementById("time-range-start").max = imagesTimeGroups.length - 1
  document.getElementById("timeOutputStart").value = imagesTimeGroups[0]

  document.getElementById("time-range-stop").min = 0
  document.getElementById("time-range-stop").max = imagesTimeGroups.length - 1
  document.getElementById("timeOutputStops").value = imagesTimeGroups[imagesTimeGroups.length - 1];

  TimeChanged = function () {
    timeStart = parseInt(document.getElementById("time-range-start").value);
    timeStop = parseInt(document.getElementById("time-range-stop").value);
    if (timeStart > timeStop) {
      document.getElementById("time-range-start").value = timeStop
      TimeChanged()
      return
    }
    document.getElementById("timeOutputStart").value = imagesTimeGroups[timeStart]
    document.getElementById("timeOutputStops").value = imagesTimeGroups[timeStop]
    //circle.setRadius(radius);
    RunDiapo(0, 0)

  }

  /* function to update the shot available under the mouse */
  map.on('mousemove', function (evt) {
    circle.setLatLng(evt.latlng);
    document.getElementById("outputText").value = (CountShotsPointer(evt.latlng.lat, evt.latlng.lng)).toString() + " Shots"
  });

  RadiusChanged = function () {
    radius = GetSpaceRange();
    if (radius > 1000) {
      document.getElementById("radiusOutput").value = parseInt(radius / 100) / 10 + ' km'
    } else {
      document.getElementById("radiusOutput").value = radius + ' meters'
    }
    circle.setRadius(radius);
    return radius
  }
}
