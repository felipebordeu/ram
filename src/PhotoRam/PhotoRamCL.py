import argparse

from PhotoRam.DataBase import DataBase
from PhotoRam.Exporters import ExportFolium

def RunFromCommand():
    parser = argparse.ArgumentParser(
        prog="PhotoRam",
        description="PhotoRam. A Simple but efficient way to explore and find your pictures",
        epilog="By Felipe Bordeu",
    )

    parser.add_argument('DataBaseDirectory')        
    parser.add_argument('--create', action='store_true')  # on/off flag
    parser.add_argument('-v', '--verbose',action='store_true')  # on/off flag
    parser.add_argument('-i', '--info',action='store_true')  # on/off flag
    parser.add_argument('-s', '--scan',action='store_true')  # on/off flag
    parser.add_argument('--fromCache',action='store_true')  # on/off flag
    parser.add_argument('--saveCache',action='store_true')  # on/off flag
    parser.add_argument('--infoNoGeoDate',action='store_true')  # on/off flag
    parser.add_argument('--AddWordToGeoloc') # " to add a word to the path geoloc
    parser.add_argument('-e','--export', help="[htmlMap]")  # on/off flag
    parser.add_argument('-a', '--add')      # option that takes a value


    args = vars(parser.parse_args())

    dataBase = DataBase(args["DataBaseDirectory"])

    dataBase.verbose = args["verbose"]

    # *************  Create ***************
    if args["create"]:
        dataBase.SaveConfigFile()

    # *************  Add Path ***************
    if args["add"] is not None:
        dataBase.AddPath(args["add"])
        dataBase.SaveConfigFile()

    # *************  Add Path ***************
    if args["AddWordToGeoloc"] is not None:
        site, geoloc = args["AddWordToGeoloc"].split(":")
        latitude, longitude = map(float, geoloc.split(","))
        dataBase.AddWordToGeoloc(site, (latitude, longitude) )
        dataBase.SaveConfigFile()


    # *************  Scan or load ***************
    shots = []
    if args["scan"] and not args["fromCache"]:
        #stopAt=400000
        print("Scanning for shots...")
        shots = dataBase.ScanPhotoRoots()
        print("Scanning for shots Done")
    elif not args["scan"] and  args["fromCache"]:
        print("Loading cache...")
        shots = dataBase.ReadCache()
        print("Loading cache Done")
    elif args["scan"] and args["fromCache"]:
        print("Cant load from cache and from scan at the same time")

    dataBase.AddGeolocationFromAlmanac(shots)
    dataBase.AddDateFromAlmanac(shots)
    # *************  Save and export ***************

    if args["export"] is not None:
        if len(shots) == 0:
            print("Exporting empty map")
            
        if args["export"].upper() ==  "htmlMap".upper():
            print("Exporting Map...")
            ExportFolium(dataBase.rootPath,shots, verbose=dataBase.verbose)
            print("Exporting Map Done")
        else:
            print(f"no export for type {args["export"]}")
            

    # *************  Save and export ***************
    if args["saveCache"]:
        if len(shots):
            dataBase.SaveCache(shots) 
        else:
            print("Empty cache nothing to save ")


    # *************  Info ***************
    if args["info"]:
        print(dataBase)
        if args["fromCache"]:
            print(f" {len(shots)} in the cache")
        if args["scan"]:
            print(f" {len(shots)} from the scan")

        if len(shots):
            print(f" {sum([shot.HasGeoLoc() for shot in shots])} shots with GeoLoc")
            print(f" {len([1 for shot in shots if shot.HasDate() ])} shots with TimeStamp")

    if args["infoNoGeoDate"]:
        import os
        from  collections import defaultdict
        print("Shot with no geolocalization ----------------------------------")
        paths = defaultdict(lambda : int(0) )
        for shot in shots:
            if shot.HasGeoLoc():
                continue
            #print(shot.imagePath)
            paths[os.path.dirname(shot.imagePath)]  += 1
        #print()
        for p, i  in sorted(paths.items(),key=lambda x :x[1]):
            #paths.items():
            print(i, p )
        
        print("Shot with no Date ---------------------------------------------")
        paths = defaultdict(lambda : int(0) )
        for shot in shots:
            if shot.HasDate():
                continue
            #print(shot.imagePath)
            paths[os.path.dirname(shot.imagePath)]  += 1
        #print()
        for p, i  in sorted(paths.items(),key=lambda x :x[1]):
            #paths.items():
            print(i, p )
    print("Done")

if __name__=='__main__':
    RunFromCommand()
    