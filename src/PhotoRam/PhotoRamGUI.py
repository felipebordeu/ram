import argparse
import os
import pathlib

import tkinter as tk
from tkinter import filedialog

from PhotoRam.DataBase import DataBase


class State:
    def __init__(self, window=None):
        self.window = window
        self.database = None
        self.shots = {}
        self.verbose = tk.BooleanVar(value=False)

    def OpenDataBase(self, directory=None):
        if directory is None:
            directory = filedialog.askdirectory()

        if len(directory) == 0:
            return
        directory = pathlib.Path(directory)
        if directory.is_dir():
            self.window.print(str(directory))

            if directory.parts[-1] == ".RamDB":
                directory = directory.parents[0]

            if not (directory / ".RamDB").is_dir():
                self.window.print(f"Not a PhotoRam Database: {directory}")
            else:
                self.database = DataBase(directory)
                self.shots = {}
                self.database.print = self.window.print
                self.window.state.PrintState()
        else:
            self.window.print(f"Directory '{directory}' does not exists")

    def CreateDataBase(self):
        directory = filedialog.askdirectory()
        if len(directory) == 0:
            return
        directory = pathlib.Path(directory)
        if os.path.isdir(directory):
            self.database = DataBase(directory)
            self.shots = {}
            self.database.print = self.window.print
            self.database.SaveConfigFile()
            self.window.state.PrintState()
        else:
            self.window.print(f"Directory '{directory}' does not exists")

    def CheckDataBase(self):
        if self.database is None:
            self.window.print("Please Open or create a DataBase first")
            return False
        return True

    def AddPath(self):
        if not self.CheckDataBase():
            return
        directory = pathlib.Path(filedialog.askdirectory())

        if directory.is_dir():
            if pathlib.Path(self.database.rootPath) in directory.parents:
                self.window.print("Cant add a path inside the database")
            else:
                self.database.AddPath(directory)
                self.database.SaveConfigFile()
                self.window.state.PrintState()
        else:
            self.window.print(f"Directory '{directory}' does not exists")

    def SetVerbose(self):
        if not self.CheckDataBase():
            return

        self.database.verbose = bool(self.verbose.get())

    def ScanShots(self):
        if not self.CheckDataBase():
            return
        self.window.print("Scanning for shots...")
        self.shots = self.database.ScanPhotoRoots()
        self.database.AddGeolocationFromAlmanac(self.shots)
        self.database.AddDateFromAlmanac(self.shots)
        self.window.print("Scanning for shots Done.")

    def SaveCache(self):
        if not self.CheckDataBase():
            return

        if len(self.shots):
            self.database.SaveCache(self.shots)
            self.window.print("Cache Saved")
        else:
            self.window.print("Empty cache nothing to save ")

    def LoadCache(self):
        if not self.CheckDataBase():
            return

        self.window.CleanOutputWindow()
        self.window.print("Loading cache...")
        self.shots = self.database.ReadCache()
        self.window.print("Loading cache Done")

        self.database.AddGeolocationFromAlmanac(self.shots)
        self.database.AddDateFromAlmanac(self.shots)
        self.window.state.PrintState(cleanScreen=False)

    def ExportHtml(self):
        from PhotoRam.Exporters import ExportFolium

        self.window.print("Exporting Map...")
        ExportFolium(self.database.rootPath, self.shots, verbose=self.database.verbose)
        self.window.print("Exporting Map Done")

    def EditManually(self):
        if not self.CheckDataBase():
            return
        file = self.database.GetDataBaseConfigFile()

        from PhotoRam.TextEditor import Window

        t = tk.Toplevel(self.window)
        textEditor = Window(t)
        textEditor.open_file_function(
            file, initialdir=os.path.join(self.database.rootPath, ".RamDB")
        )

    def PrintState(self, cleanScreen=True):
        if cleanScreen:
            self.window.CleanOutputWindow()
        self.window.print(str(self.database))
        self.window.print(" - * - * - * - * - * -")
        self.window.print(f" {len(self.shots)} shots in the cache.")

    def ExportToWebPage(self):
        if not self.CheckDataBase():
            return

        from PhotoRam.Exporters import ExportFolium

        ExportFolium(
            self.database.rootPath,
            self.shots,
            verbose=self.database.verbose,
            print=self.window.print,
        )


class WordToGeoToTimeStamp(tk.Frame):
    def __init__(self, master=None, mainWindow=None, toGeo=True):

        super().__init__(master)
        self.master = master
        if toGeo:
            self.master.title("PhotoRam Word To Geolocalization")
            label = "-- Word To Geolocalization --"
            label2 = "Add Word to Geolocalization"
            callback = self.AddWordToGeoloc
        else:
            self.master.title("PhotoRam Word To TimeStamp")
            label = "-- Word To Timestamp --"
            label2 = "Add Word to TimeStamp"
            callback = self.AddWordToTimeStamp

        self.mainWindow: PhotoRamApp = mainWindow
        rowcpt = 0
        self.geo_label = tk.Label(self, text=label)
        self.geo_label.grid(column=0, row=rowcpt, columnspan=2, sticky="we")
        rowcpt += 1
        self.Keyword = tk.Entry(self)
        self.Keyword.grid(column=0, row=rowcpt, columnspan=2, sticky="we")
        rowcpt += 1
        if toGeo:
            self.WordToGeoloc_GeolocLatitude = tk.Entry(self)
            self.WordToGeoloc_GeolocLatitude.grid(column=0, row=rowcpt)
            self.WordToGeoloc_GeolocLongitude = tk.Entry(self)
            self.WordToGeoloc_GeolocLongitude.grid(column=1, row=rowcpt)
        else:
            self.WordToEntry1 = tk.Entry(self)
            self.WordToEntry1.grid(column=0, row=rowcpt, columnspan=2)

        rowcpt += 1

        self.addPath_button = tk.Button(self, text=label2, command=callback)

        self.addPath_button.grid(column=0, row=rowcpt, columnspan=2)
        rowcpt += 1

        self.pack(side="top", fill="both", expand=True, padx=50, pady=50)

    def AddWordToGeoloc(self):
        if not self.mainWindow.state.CheckDataBase():
            return
        word = self.Keyword.get()
        latitude = self.WordToGeoloc_GeolocLatitude.get()

        longitude = self.WordToGeoloc_GeolocLongitude.get()
        if (
            len(word)
            and len(latitude)
            and len(longitude)
            and float(latitude)
            and float(longitude)
        ):
            self.mainWindow.state.database.AddWordToGeoloc(word, [latitude, longitude])
            self.mainWindow.state.database.SaveConfigFile()
            self.mainWindow.state.PrintState()

    def AddWordToTimeStamp(self):
        if not self.mainWindow.state.CheckDataBase():
            return
        word = self.Keyword.get()
        entry1 = self.WordToEntry1.get()
        if len(word) and len(entry1):
            self.mainWindow.state.database.AddWordToDate(word, entry1)
            self.mainWindow.state.database.SaveConfigFile()
            self.mainWindow.state.PrintState()

    def PrintState(self):
        if not self.mainWindow.state.CheckDataBase():
            return
        self.mainWindow.state.PrintState()


class PhotoRamApp(tk.Frame):

    def __fillMenu(self, menu, actions):

        for name, action in actions.items():

            if name == "separator":
                menu.add("separator")
            elif name == "checkbutton":
                menu.add("checkbutton", cnf=action)
            else:
                menu.add_command(label=name, command=action)

    def __fillFileMenu(self):
        actions = {
            "Open DataBase": self.state.OpenDataBase,
            "Create DataBase": self.state.CreateDataBase,
            "Print DateBase": self.state.PrintState,
            "separator": None,
            "Exit": self.master.destroy,
        }

        self.__fillMenu(self.file_menu, actions)

    def __fillEditMenu(self):
        actions = {
            "Add Path": self.state.AddPath,
            "Add Word to Geolocalization": self.OpenWordToGeoForm,
            "Add Word to TimeStamp": self.OpenWordToTimeStampForm,
            "separator": None,
            "checkbutton": {
                "label": "Verbose",
                "command": self.state.SetVerbose,
                "variable": self.state.verbose,
            },
            "Edit DataBase Manually": self.state.EditManually,
        }

        self.__fillMenu(self.edit_menu, actions)

    def __fillFindMenu(self):
        actions = {
            "Scan shots": self.state.ScanShots,
            "Load From Cache": self.state.LoadCache,
            "separator": None,
            "Save Cache": self.state.SaveCache,
        }
        self.__fillMenu(self.find_menu, actions)

    def __fillExportMenu(self):
        actions = {"Export To WebPage": self.state.ExportToWebPage}
        self.__fillMenu(self.export_menu, actions)

    def __fillHelpMenu(self):
        actions = {}
        self.__fillMenu(self.help_menu, actions)

    def __init__(self, master=None, baseDirectory=""):

        super().__init__(master)
        # self.master = master

        self.master.title("PhotoRam")
        self.state = State(self)

        menubar = tk.Menu(self.master)
        self.master.config(menu=menubar)

        self.file_menu = tk.Menu(menubar, tearoff=False)
        menubar.add_cascade(label="File", menu=self.file_menu)
        self.__fillFileMenu()

        self.edit_menu = tk.Menu(menubar, tearoff=False)
        menubar.add_cascade(label="Edit", menu=self.edit_menu)
        self.__fillEditMenu()

        self.find_menu = tk.Menu(menubar, tearoff=False)
        menubar.add_cascade(label="Find", menu=self.find_menu)
        self.__fillFindMenu()

        self.export_menu = tk.Menu(menubar, tearoff=False)
        menubar.add_cascade(label="Export", menu=self.export_menu)
        self.__fillExportMenu()

        self.help_menu = tk.Menu(menubar, tearoff=False)
        menubar.add_cascade(label="Help", menu=self.help_menu)
        self.__fillHelpMenu()

        self.master.geometry("800x300")

        self.output_text = tk.Text(
            self, height=200, width=200
        )  # Use Text widget insted of Listbox
        self.output_text.pack(side=tk.LEFT, fill=tk.Y, expand=True)

        self.scrollbar = tk.Scrollbar(self, orient="vertical")
        self.scrollbar.config(command=self.output_text.yview)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y, expand=True)

        # change all occurances of self.listNodes to self.text
        self.output_text.config(yscrollcommand=self.scrollbar.set)

        self.pack()
        self.state.OpenDataBase(baseDirectory)

    def OpenWordToGeoForm(self):
        t = tk.Toplevel(self)
        WordToGeoToTimeStamp(master=t, mainWindow=self, toGeo=True)

    def OpenWordToTimeStampForm(self):
        t = tk.Toplevel(self)
        WordToGeoToTimeStamp(master=t, mainWindow=self, toGeo=False)

    def print(self, text):
        # self.var.set(text)
        # self.output_text.delete(0, tk.END)
        self.output_text.insert(tk.END, text + "\n")

    def CleanOutputWindow(self):
        if self.output_text.size():
            self.output_text.delete(1.0, tk.END)


def RunGUI():

    parser = argparse.ArgumentParser(
        prog="PhotoRamGUI",
        description="PhotoRam. A Simple but efficient way to explore and find your pictures",
        epilog="By Felipe Bordeu",
    )
    parser.add_argument("--DataBaseDirectory", default="", required=False)
    args = parser.parse_args()

    PhotoRamApp(baseDirectory=args.DataBaseDirectory).mainloop()


if __name__ == "__main__":
    RunGUI()
